CrystFEL - Crystallography with a FEL
-------------------------------------

Copyright © 2012-2018 Deutsches Elektronen-Synchrotron DESY,
                      a research centre of the Helmholtz Association.

Authors:
  Thomas White <taw@physics.org>
  Richard Kirian <rkirian@asu.edu>
  Kenneth Beyerlein <kenneth.beyerlein@desy.de>
  Andrew Aquila <andrew.aquila@cfel.de>
  Andrew Martin <andrew.martin@desy.de>
  Lorenzo Galli <lorenzo.galli@desy.de>
  Chun Hong Yoon <chun.hong.yoon@desy.de>
  Kenneth Beyerlein <kenneth.beyerlein@desy.de>
  Karol Nass <karol.nass@desy.de>
  Nadia Zatsepin <nadia.zatsepin@asu.edu>
  Anton Barty <anton.barty@desy.de>
  Cornelius Gati <cornelius.gati@desy.de>
  Fedor Chervinskii <fedor.chervinskii@gmail.com>
  Alexandra Tolstikova <alexandra.tolstikova@desy.de>
  Wolfgang Brehm <wolfgang.brehm@gmail.com>
  Valerio Mariani <valerio.mariani@desy.de>
  Parker de Waal <Parker.deWaal@vai.org>
  Takanori Nakane <nakane.t@gmail.com>
  Keitaro Yamashita <k.yamashita@spring8.or.jp>
  Mamoru Suzuki <mamoru.suzuki@protein.osaka-u.ac.jp>
  Thomas Grant <tgrant@hwi.buffalo.edu>
  Steve Aplin <steve.aplin@desy.de>
  Oleksandr Yefanov <oleksandr.yefanov@desy.de>
  Helen Ginn  <helen@strubi.ox.ac.uk>
  Nicolas Riebesel <nicolas.riebesel@tuhh.de>
  Yaroslav Gevorkov <yaroslav.gevorkov@desy.de>

CrystFEL is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

CrystFEL is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
CrystFEL.  If not, see <http://www.gnu.org/licenses/>.


Thank you for reading the documentation. :-)

CrystFEL is a suite of programs for processing (and simulating) Bragg
diffraction data from "serial crystallography" experiments, often (but not
always) performed using an X-ray Free-Electron Laser.  Some of the particular
characteristics of such data which call for a specialised software suite are:

- The sliced, rather than integrated, measurement of intensity data.  Many, if
   not all reflections are partially integrated.

- Many patterns (thousands) are required - high throughput is needed.

- The crystal orientations in each pattern are random and uncorrelated, meaning
   that:

- Merging into lower symmetry point groups may require the resolution of
   indexing ambiguities.


CrystFEL includes programs for simulating and processing patterns subject to the
above characteristics.  The main programs are:

	- indexamajig, a "batch indexer" and data reduction program.

	- process_hkl, for merging per-pattern lists of intensities into a
	               single reflection list.

	- cell_explorer, for examining distributions of lattice parameters.

	- ambigator, a tool for resolving indexing ambiguities,

	- geoptimiser, for refining detector geometry.

	- whirligig, for finding multiple shots from single crystals.

	- partialator, for merging patterns more accurately (and much more
	               slowly) using post refinement.

In addition, there is also:

	- get_hkl, for doing various simple operations on reflection lists.

	- compare_hkl, for working out the differences (e.g. a q-dependent
	               scaling factor) between two lists of reflections.

	- check_hkl, for determining things like completeness.

	- pattern_sim, for simulating patterns.

	- partial_sim, for calculating partial reflection intensities.

	- hdfsee, a simple viewer for images stored in HDF5 format.

	- render_hkl, for turning reflection lists into pretty graphics.

	- list_events, for creating event lists from multi-event files

There is also a folder full of scripts for achieving many related tasks.

CrystFEL mostly works with images stored in HDF5 format, unit cell data in PDB
format, and reflection lists in plain text format (i.e. not MTZ).  There are
scripts for converting both ways between plain text reflection lists and MTZ
files.

Standard "man" pages are provided for documentation, or you can invoke any
program with the argument "--help" to get a summary of options.  There are some
example geometry and beam description files in doc/examples, and development
documentation created using GTK-Doc.


Installation
------------

$ mkdir build
$ cd build
$ cmake ..
$ make
$ sudo make install

At a minimum, you will need the HDF5 library (version 1.8.0 or later) and the
GNU Scientific Library (GSL).  For a full installation, you will also need
libTIFF, libPNG, Cairo, FFTW3, CBFLib and GTK2.  All of these should be
installed from your distribution's package manager.  Be sure to install the
development files for each package, which will have a name like "gtk+-2.0-dev"
or "gtk+-2.0-devel", depending on which distribution you use.

If you get an error mentioning fftw3 and telling you to recompile with -fPIC,
then your version of FFTW3 is not compiled in a suitable way.  You'll need to
install it again (from source) adding "--enable-shared" to its ./configure
command line.


Funding acknowledgements
------------------------

Development of CrystFEL is primarily funded by the Helmholtz Association via
programme-oriented funds.

Additional funding for CrystFEL is provided by "X-Probe", a project of the
European Union's 2020 Research and Innovation Program Under the Marie
Skłodowska-Curie grant agreement 637295 (2015-2018).

Additional funding for CrystFEL is provided by the BMBF German-Russian
Cooperation "SyncFELMed", grant 05K14CHA (2014-2017).

Past funding for CrystFEL has been received from BioStruct-X, a project funded
by the Seventh Framework Programme (FP7) of the European Commission.
