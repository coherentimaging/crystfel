# Install script for directory: /home/hoganlap/crystfel/libcrystfel

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcrystfel.so.0.7.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcrystfel.so.10"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcrystfel.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "/usr/lib/x86_64-linux-gnu/hdf5/serial/lib:/usr/local/lib")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES
    "/home/hoganlap/crystfel/bin/libcrystfel/libcrystfel.so.0.7.0"
    "/home/hoganlap/crystfel/bin/libcrystfel/libcrystfel.so.10"
    "/home/hoganlap/crystfel/bin/libcrystfel/libcrystfel.so"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcrystfel.so.0.7.0"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcrystfel.so.10"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libcrystfel.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHANGE
           FILE "${file}"
           OLD_RPATH "/usr/lib/x86_64-linux-gnu/hdf5/serial/lib:/usr/local/lib:"
           NEW_RPATH "/usr/lib/x86_64-linux-gnu/hdf5/serial/lib:/usr/local/lib")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/crystfel" TYPE FILE FILES
    "/home/hoganlap/crystfel/libcrystfel/src/hdf5-file.h"
    "/home/hoganlap/crystfel/libcrystfel/src/reflist.h"
    "/home/hoganlap/crystfel/libcrystfel/src/symmetry.h"
    "/home/hoganlap/crystfel/libcrystfel/src/cell.h"
    "/home/hoganlap/crystfel/libcrystfel/src/reflist-utils.h"
    "/home/hoganlap/crystfel/libcrystfel/src/thread-pool.h"
    "/home/hoganlap/crystfel/libcrystfel/src/statistics.h"
    "/home/hoganlap/crystfel/libcrystfel/src/utils.h"
    "/home/hoganlap/crystfel/libcrystfel/src/detector.h"
    "/home/hoganlap/crystfel/libcrystfel/src/geometry.h"
    "/home/hoganlap/crystfel/libcrystfel/src/peakfinder8.h"
    "/home/hoganlap/crystfel/libcrystfel/src/peaks.h"
    "/home/hoganlap/crystfel/libcrystfel/src/stream.h"
    "/home/hoganlap/crystfel/libcrystfel/src/render.h"
    "/home/hoganlap/crystfel/libcrystfel/src/index.h"
    "/home/hoganlap/crystfel/libcrystfel/src/image.h"
    "/home/hoganlap/crystfel/libcrystfel/src/filters.h"
    "/home/hoganlap/crystfel/libcrystfel/src/dirax.h"
    "/home/hoganlap/crystfel/libcrystfel/src/mosflm.h"
    "/home/hoganlap/crystfel/libcrystfel/src/cell-utils.h"
    "/home/hoganlap/crystfel/libcrystfel/src/integer_matrix.h"
    "/home/hoganlap/crystfel/libcrystfel/src/crystal.h"
    "/home/hoganlap/crystfel/libcrystfel/src/xds.h"
    "/home/hoganlap/crystfel/libcrystfel/src/predict-refine.h"
    "/home/hoganlap/crystfel/libcrystfel/src/integration.h"
    "/home/hoganlap/crystfel/libcrystfel/src/histogram.h"
    "/home/hoganlap/crystfel/libcrystfel/src/events.h"
    "/home/hoganlap/crystfel/libcrystfel/src/asdf.h"
    "/home/hoganlap/crystfel/libcrystfel/src/felix.h"
    "/home/hoganlap/crystfel/libcrystfel/src/peakfinder8.h"
    "/home/hoganlap/crystfel/libcrystfel/src/taketwo.h"
    "/home/hoganlap/crystfel/libcrystfel/src/xgandalf.h"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/hoganlap/crystfel/bin/libcrystfel/crystfel.pc")
endif()

