/* config.h for libcrystfel */

#define HAVE_CLOCK_GETTIME
#define HAVE_CPU_AFFINITY
#define HAVE_FFTW
#define HAVE_XGANDALF
/* #undef HAVE_FDIP */
#define HAVE_CURSES

/* We avoid adding the full path to cbf.h, because CBFlib unhelpfully installs
 * some conflicting HDF5 headers which we want to keep out of the include path.
 * Unfortunately, sometimes CBFlib installs cbf/cbf.h, other times cbflib/cbf.h.
 * These defines tell whether we have CBFlib at all, and if so, what to #include */
#define HAVE_CBFLIB
#define HAVE_CBF_CBF_H
/* #undef HAVE_CBFLIB_CBF_H */

#define HAVE_FORKPTY_PTY_H
/* #undef HAVE_FORKPTY_UTIL_H */

#define CRYSTFEL_VERSIONSTRING "0.7.0+5601f027"
