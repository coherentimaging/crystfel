# CMake generated Testfile for 
# Source directory: /home/hoganlap/crystfel/tests
# Build directory: /home/hoganlap/crystfel/bin/tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(process_hkl_check_1 "/home/hoganlap/crystfel/tests/process_hkl_check_1" "/home/hoganlap/crystfel/bin/process_hkl")
add_test(process_hkl_check_2 "/home/hoganlap/crystfel/tests/process_hkl_check_2" "/home/hoganlap/crystfel/bin/process_hkl")
add_test(process_hkl_check_3 "/home/hoganlap/crystfel/tests/process_hkl_check_3" "/home/hoganlap/crystfel/bin/process_hkl")
add_test(process_hkl_check_4 "/home/hoganlap/crystfel/tests/process_hkl_check_4" "/home/hoganlap/crystfel/bin/process_hkl")
add_test(partialator_merge_check_1 "/home/hoganlap/crystfel/tests/partialator_merge_check_1" "/home/hoganlap/crystfel/bin/partialator")
add_test(partialator_merge_check_2 "/home/hoganlap/crystfel/tests/partialator_merge_check_2" "/home/hoganlap/crystfel/bin/partialator")
add_test(partialator_merge_check_3 "/home/hoganlap/crystfel/tests/partialator_merge_check_3" "/home/hoganlap/crystfel/bin/partialator")
add_test(ambi_check "ambi_check")
add_test(cell_check "cell_check")
add_test(centering_check "centering_check")
add_test(integration_check "integration_check")
add_test(list_check "list_check")
add_test(prediction_gradient_check "prediction_gradient_check")
add_test(prof2d_check "prof2d_check")
add_test(ring_check "ring_check")
add_test(symmetry_check "symmetry_check")
add_test(transformation_check "transformation_check")
add_test(gpu_sim_check "gpu_sim_check")
