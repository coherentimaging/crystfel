/* config.h for CrystFEL main programs */

#define HAVE_GTK
#define HAVE_GDKPIXBUF
#define HAVE_GDK
#define HAVE_TIFF
#define HAVE_CAIRO
#define HAVE_OPENCL
#define HAVE_CL_CL_H
#define HAVE_CLOCK_GETTIME

#define PACKAGE_VERSION "0.7.0+5601f027"

#define CRYSTFEL_VERSIONSTRING "0.7.0+5601f027"

#define CRYSTFEL_BOILERPLATE "License GPLv3+: GNU GPL version 3 or later"\
     " <http://gnu.org/licenses/gpl.html>.\n"\
     "This is free software: you are free to change and redistribute it.\n"\
     "There is NO WARRANTY, to the extent permitted by law.\n\n"\
     "Written by Thomas White and others."
